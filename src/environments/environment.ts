// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDagg5q45ql54n50PXLH9EDK25UMGFV5Js',
    authDomain: 'photogallery-d1e22.firebaseapp.com',
    projectId: 'photogallery-d1e22',
    storageBucket: 'photogallery-d1e22.appspot.com',
    messagingSenderId: '876251103300',
    appId: '1:876251103300:web:b6cc57a48b5962c529fc11'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
