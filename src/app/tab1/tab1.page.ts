import { Component , OnInit} from '@angular/core';
import { PhotoService, UserPhoto} from '../services/photo.service';
import { ActionSheetController } from '@ionic/angular';
import { DataBaseService } from '../services/data-base.service';
import { ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{

  openModel: boolean;
  title: string;
  description: string;
  email: string;

  constructor(private toasyCtrl: ToastController,public photoService: PhotoService, public actionSheetController: ActionSheetController, public database: DataBaseService, private authService: AuthService ) {
    this.authService.getUserLogged().subscribe(res=>{
      this.email = res.email
    })
  }

  async ngOnInit() {
    await this.photoService.loadSaved();
  }

  public async closeModel(){
    this.openModel =false;
  }

  public async changeImage(photo: UserPhoto, position: number){
    this.photoService.changeImage(photo, position, this.title, this.description);
    this.closeModel();
  }

  public async showActionSheet(photo: UserPhoto, position: number) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.photoService.deleteImage(photo, position);
        }
      }, {
        text: 'Change',
        icon: 'image-outline',
        role: 'change',
        handler: () => {
          this.openModel = true;
        }
      },{
        text: 'Save',
        icon: 'save-outline',
        role: 'save',
        handler: () => {
          this.insert(position);
          
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
          }
      }]
    });
    await actionSheet.present();
  }

  insert(position: number){
    this.database.create(this.email, this.photoService.photos[position]).then(res=>{
      this.presentToast('Image saved successfully');
    }).catch(err=>{
      this.presentToast('Error, the picture didnt save successfully');
    })
  }

  async presentToast(mess: string){
    const toast = await this.toasyCtrl.create({
      message: mess,
      duration: 1000
    });
    toast.present();
  }
}

