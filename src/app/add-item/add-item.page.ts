import { Component, OnInit } from '@angular/core';
import { PhotoService } from '../services/photo.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.page.html',
  styleUrls: ['./add-item.page.scss'],
})
export class AddItemPage implements OnInit {

  title: string;
  description: string;
  date: Date;

  constructor(public photoService: PhotoService,private router: Router) { }

  ngOnInit() {
  }

  addPhotoToGallery() {
    this.photoService.addNewToGallery(this.title, this.description, this.date);
  }


}
