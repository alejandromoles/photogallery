import { Injectable } from '@angular/core';
import { Camera, CameraResultType, CameraSource, Photo } from '@capacitor/camera';
import { Capacitor } from '@capacitor/core';
import { Filesystem, Directory } from '@capacitor/filesystem';
import { Storage } from '@capacitor/storage';
import { Platform } from '@ionic/angular';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class PhotoService {
  public photos: UserPhoto[] = [];
  private photoStorage = 'photos';
  private platform: Platform;


  constructor(platform: Platform, private router: Router) {
    this.platform = platform;
   }

  public async addNewToGallery(title: string, description: string, date: Date) {
    // Take a photo
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });

    const savedImageFile = await this.savePicture(capturedPhoto, title, description, date);
    this.photos.push(savedImageFile);

    Storage.set({
      key: this.photoStorage,
      value: JSON.stringify(this.photos),
    });

    this.router.navigateByUrl(`/tabs`)
  }

  public async deleteImage(photo: UserPhoto, position: number){
    this.photos.splice(position, 1);

    Storage.set({
      key: this.photoStorage,
      value: JSON.stringify(this.photos)
    });

    const filename = photo.filepath
                      .substr(photo.filepath.lastIndexOf('/') + 1);

    await Filesystem.deleteFile({
      path: filename,
      directory: Directory.Data
    });
  }

  public async changeImage(photo: UserPhoto, position: number, title: string, description: string){
    const capturedPhoto = await Camera.getPhoto({
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
      quality: 100
    });

    const savedImageFile = await this.savePicture(capturedPhoto, title, description, new Date());

    this.photos[position].filepath = savedImageFile.filepath;
    this.photos[position].webviewPath = savedImageFile.webviewPath;
    this.photos[position].imageTitle = savedImageFile.imageTitle;
    this.photos[position].content = savedImageFile.content;
    this.photos[position].imageDate = savedImageFile.imageDate;

    Storage.set({
      key: this.photoStorage,
      value: JSON.stringify(this.photos),
    });
  }

  public async loadSaved() {
    // Retrieve cached photo array data
    const photoList = await Storage.get({ key: this.photoStorage });
    this.photos = JSON.parse(photoList.value) || [];

    if(!this.platform.is('hybrid')){
      // eslint-disable-next-line prefer-const
      for (let photo of this.photos) {
        // Read each saved photo's data from the Filesystem
        const readFile = await Filesystem.readFile({
          path: photo.filepath,
          directory: Directory.Data,
        });

        // Web platform only: Load the photo as base64 data
        photo.webviewPath = `data:image/jpeg;base64,${readFile.data}`;
      }
    }
  }

  private async savePicture(photo: Photo, title: string, description: string, date: Date) {
    const base64Data = await this.readAsBase64(photo);

    // Write the file to the data directory
    const fileName = new Date().getTime() + '.jpeg';
    const savedFile = await Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: Directory.Data
    });

    if (this.platform.is('hybrid')) {
      // Display the new image by rewriting the 'file://' path to HTTP
      // Details: https://ionicframework.com/docs/building/webview#file-protocol
      return {
        filepath: savedFile.uri,
        webviewPath: Capacitor.convertFileSrc(savedFile.uri),
        imageTitle: title,
        content: description,
        imageDate: date
      };
    }else{
      // Use webPath to display the new image instead of base64 since it's
      // already loaded into memory
      return {
        filepath: fileName,
        webviewPath: photo.webPath,
        imageTitle: title,
        content: description,
        imageDate: date
      };
    }
  }

  private async readAsBase64(photo: Photo) {
    if (this.platform.is('hybrid')) {
      // Read the file into base64 format
      const file = await Filesystem.readFile({
        path: photo.path
      });

      return file.data;
    }else{
      // Fetch the photo, read as a blob, then convert to base64 format
      const response = await fetch(photo.webPath);
      const blob = await response.blob();
      return await this.convertBlobToBase64(blob) as string;
    }
  }

  private convertBlobToBase64 = (blob: Blob) => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onerror = reject;
    reader.onload = () => {
        resolve(reader.result);
    };
    reader.readAsDataURL(blob);
  });
}
export interface UserPhoto {
  filepath: string;
  webviewPath: string;
  imageTitle: string;
  content: string;
  imageDate: Date;
}


