import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/compat/auth';
import firebase from 'firebase/compat/app';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afauth: AngularFireAuth) { }

 

  async register(email, pass){
    try {
      return await this.afauth.createUserWithEmailAndPassword(email, pass);
    } catch (e) {
      if(e.message = "Firebase: The email address is already in use by another account. (auth/email-already-in-use)."){
        return 1;
      }else{
        return 0
      }
    }
  }

  async login(email: string, pass: string){
    try{
      //all is correct
      return await this.afauth.signInWithEmailAndPassword(email,pass);
    }catch(err){
      switch(err.message){
        //this error occurred when the password that is in the database is not correct for the email
        case 'Firebase: The password is invalid or the user does not have a password. (auth/wrong-password).':
          return 1;
        //this error occurred when the email wich you are trying to log in is not registered in the database
        case 'Firebase: There is no user record corresponding to this identifier. The user may have been deleted. (auth/user-not-found).':
          return 2;
      }
    }
  }

  async loginGoogle(email: string, pass: string){
    try{
      return await this.afauth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    }catch(err){
      return null;
    }
  }

  getUserLogged(){
    return this.afauth.authState;
  }

  logOut(){
    this.afauth.signOut;
  }



}
