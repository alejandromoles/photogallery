import { Injectable } from '@angular/core';
import { AngularFirestore } from  '@angular/fire/compat/firestore';


@Injectable({
  providedIn: 'root'
})

export class DataBaseService {

constructor(private firestore: AngularFirestore) { }

  async create(collecion, data){
    return await this.firestore.collection(collecion).add(data);
  }

  async getAll(collection){
    try{
      return await this.firestore.collection(collection).snapshotChanges();
    }catch(err){
      console.log(err)
    }
  }

  async delete(collection, id){
    try{
      return await this.firestore.collection(collection).doc(id).delete();
    }catch(err){
      console.log(err)
    }
  }


}
