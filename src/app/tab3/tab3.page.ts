import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  userLogged = this.authService.getUserLogged();
  constructor(private authService: AuthService) {
    this.authService.getUserLogged().subscribe(res=>{
    })
  }

  logOut(){
    this.authService.logOut
  }
}
