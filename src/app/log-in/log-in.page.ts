import { Component, OnInit,  ViewChild } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AuthService } from '../services/auth.service';
import {Router} from '@angular/router';
import  {FormGroup, FormBuilder, Validators}  from "@angular/forms";
import { AlertController, LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.page.html',
  styleUrls: ['./log-in.page.scss'],
})
export class LogInPage implements OnInit {

  credentials: FormGroup;

  user = {
    email: '',
    pass: ''
  };

  constructor(private authService: AuthService,
              private toasyCtrl: ToastController,
              private router: Router,
              private fb: FormBuilder,
              private loadingController: LoadingController,
              private alertController: AlertController,) { }

  get email() {
    return this.credentials.get('email');
  }

  get password() {
    return this.credentials.get('password');
  }
  
  ngOnInit() {
    this.credentials = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  async register(){
    const loading = await this.loadingController.create();
    await loading.present();


    this.authService.register(this.credentials.value.email, this.credentials.value.password).then(res=>{
      console.log(res)
      switch (res){
        case 1:
          loading.dismiss();
          this.presentToast('Another account has registered with this email');
          break;

        default:
          loading.dismiss();
          this.presentToast('Sig in successfully');
          break;
      }
    })
  }

  async logInGoogle(){
    const loading = await this.loadingController.create();
    await loading.present();

    const {email, pass} = this.user;
    this.authService.loginGoogle(email,pass).then(res=>{
      if(res === null){
        loading.dismiss();
        this.presentToast('An error has occurred');
      }else{
        loading.dismiss();
        this.presentToast('Log in with google');
        this.router.navigateByUrl(`/tabs`)
      }
    });
  }

  async logInEmail(){
    const loading = await this.loadingController.create();
    await loading.present();

    this.authService.login(this.credentials.value.email,this.credentials.value.password).then(res=>{
      switch (res){
        case 1:
          //error in the password
          loading.dismiss();
          this.presentToast('The password is incorrect');
          break;
        case 2:
          //error in the email, the email is not registered
          loading.dismiss();
          this.presentToast('The email is not registered in the database');
          break;
        default:
          //all correct
          loading.dismiss();
          this.presentToast('Log in with email and password');
          this.router.navigateByUrl(`/tabs`);
          break;
      }
    })
  }


  async presentToast(mess: string){
    const toast = await this.toasyCtrl.create({
      message: mess,
      duration: 1000
    });
    toast.present();
  }

}
