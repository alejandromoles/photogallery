import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { LogInPageRoutingModule } from './log-in-routing.module';

import { LogInPage } from './log-in.page';
import {AngularFireModule} from '@angular/fire/compat';
import { environment } from 'src/environments/environment';
import  {FormsModule, ReactiveFormsModule} from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    LogInPageRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [LogInPage]
})
export class LogInPageModule {}
