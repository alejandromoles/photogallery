import { AfterViewChecked, AfterViewInit, Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { PhotoService, UserPhoto } from '../services/photo.service';
import { ActionSheetController,ToastController } from '@ionic/angular';
import { DataBaseService } from '../services/data-base.service';
import { AuthService } from '../services/auth.service';



@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit, OnChanges{

  email: string;
  public listPictures = [];

  constructor(private toasyCtrl: ToastController,public database: DataBaseService, private authService: AuthService,public actionSheetController: ActionSheetController) {}

  ngOnChanges(changes: SimpleChanges): void {
    this.listPictures = []
    this.authService.getUserLogged().subscribe(res=>{
      this.email = res.email;
      this.getAll();
    })
  }

  ngOnInit(){
    this.authService.getUserLogged().subscribe(res=>{
      this.email = res.email;
      this.getAll();
    })
  }
 
  async getAll(){
    await this.database.getAll(this.email).then(res =>{ 
      res.subscribe(listPicturesRef => {
        listPicturesRef.forEach(photoRef=>{ 
          this.listPictures = listPicturesRef.map(photoRef =>{
            let photo = photoRef.payload.doc.data();
            photo['id'] = photoRef.payload.doc.id;
            return photo;
          })
        })
      })
    })
  }

  public async showActionSheet(picture: Pictures) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Photos',
      buttons: [{
        text: 'Delete',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.database.delete(this.email,picture.id).then(res=>{
            this.presentToast('Picture delete');
          }).catch(err=>{
            this.presentToast('An error has occurred');
          });
        }
      },{
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          // Nothing to do, action sheet is automatically closed
          }
      }]
    });
    await actionSheet.present();
  }

  async presentToast(mess: string){
    const toast = await this.toasyCtrl.create({
      message: mess,
      duration: 1000
    });
    toast.present();
  }
}

export interface Pictures{
  filepath: string;
  webviewPath: string;
  imageTitle: string;
  content: string;
  imageDate: Date;
  id:string;
}



