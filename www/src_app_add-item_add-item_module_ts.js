"use strict";
(self["webpackChunkapp"] = self["webpackChunkapp"] || []).push([["src_app_add-item_add-item_module_ts"],{

/***/ 1226:
/*!*****************************************************!*\
  !*** ./src/app/add-item/add-item-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddItemPageRoutingModule": () => (/* binding */ AddItemPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _add_item_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-item.page */ 2621);




const routes = [
    {
        path: '',
        component: _add_item_page__WEBPACK_IMPORTED_MODULE_0__.AddItemPage
    }
];
let AddItemPageRoutingModule = class AddItemPageRoutingModule {
};
AddItemPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], AddItemPageRoutingModule);



/***/ }),

/***/ 5606:
/*!*********************************************!*\
  !*** ./src/app/add-item/add-item.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddItemPageModule": () => (/* binding */ AddItemPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 3819);
/* harmony import */ var _add_item_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-item-routing.module */ 1226);
/* harmony import */ var _add_item_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-item.page */ 2621);







let AddItemPageModule = class AddItemPageModule {
};
AddItemPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _add_item_routing_module__WEBPACK_IMPORTED_MODULE_0__.AddItemPageRoutingModule
        ],
        declarations: [_add_item_page__WEBPACK_IMPORTED_MODULE_1__.AddItemPage]
    })
], AddItemPageModule);



/***/ }),

/***/ 2621:
/*!*******************************************!*\
  !*** ./src/app/add-item/add-item.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AddItemPage": () => (/* binding */ AddItemPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _add_item_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./add-item.page.html?ngResource */ 6031);
/* harmony import */ var _add_item_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./add-item.page.scss?ngResource */ 1816);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _services_photo_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/photo.service */ 1957);





let AddItemPage = class AddItemPage {
    constructor(photoService) {
        this.photoService = photoService;
    }
    ngOnInit() {
    }
    addPhotoToGallery() {
        this.photoService.addNewToGallery(this.title, this.description, this.date);
    }
};
AddItemPage.ctorParameters = () => [
    { type: _services_photo_service__WEBPACK_IMPORTED_MODULE_2__.PhotoService }
];
AddItemPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-add-item',
        template: _add_item_page_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_add_item_page_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], AddItemPage);



/***/ }),

/***/ 1816:
/*!********************************************************!*\
  !*** ./src/app/add-item/add-item.page.scss?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJhZGQtaXRlbS5wYWdlLnNjc3MifQ== */";

/***/ }),

/***/ 6031:
/*!********************************************************!*\
  !*** ./src/app/add-item/add-item.page.html?ngResource ***!
  \********************************************************/
/***/ ((module) => {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-row>\n        <ion-button [routerLink]=\"['..']\">\n          <ion-icon name=\"arrow-back-outline\" ></ion-icon>\n        </ion-button>\n      <ion-title>\n        Add Picture\n      </ion-title>\n    </ion-row>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content [fullscreen] = \"true\">\n  <ion-grid>\n    <ion-list >\n        <ion-item>\n          <ion-label position=\"floating\" >TITLE</ion-label>\n          <ion-input type=\"text\" \n              [(ngModel)]=\"title\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"floating\" >DESCRIPTION</ion-label>\n          <ion-input type=\"text\" [(ngModel)]=\"description\"></ion-input>\n        </ion-item>\n        <ion-item>\n          <ion-label position=\"floating\" >DATE</ion-label>\n          <ion-input type=\"DATE\" [(ngModel)]=\"date\"></ion-input>\n        </ion-item>\n    </ion-list>\n  </ion-grid>\n\n  <ion-fab vertical=\"bottom\" horizontal=\"center\" slot=\"fixed\">\n    <ion-fab-button (click)=\"addPhotoToGallery()\">\n      <ion-icon name=\"camera\"></ion-icon>\n    </ion-fab-button>\n  </ion-fab>\n</ion-content>";

/***/ })

}]);
//# sourceMappingURL=src_app_add-item_add-item_module_ts.js.map